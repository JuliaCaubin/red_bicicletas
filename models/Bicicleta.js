let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let bicicletaSchema = new Schema({
    bicicletaID: Number,
    color: String,
    modelo: String,
    ubicacion: { type: [Number], index: true}
});

//constructor pasandole por parametros id, color, modelo, ubicacion
let Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

bicicletaSchema.statics.allBicis = function (cb) {
    return this.find( {}, cb);
};

bicicletaSchema.statics.add = function (aBici, cb) {
    return this.create(aBici, cb);
};

bicicletaSchema.statics.removeById = function (aBiciId, cb){
    return this.findByIdAndDelete(aBiciId, cb);
}

bicicletaSchema.statics.findById = function (aBiciId, bici, cb){
    return this.findByIdAndUpdate(aBiciId, bici, cb);
}


/*Bicicleta.allBicis = [];
Bicicleta.add = function (bici){
    this.allBicis.push(bici);
}
let a = new Bicicleta(1, "Rojo", "Trek", [28.503789, -13.853296]);
let b = new Bicicleta(2, "Azul", "Orbea", [28.501367, -13.853476]);
Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.removeById = function(aBiciId) {
    //recorre el array
    for (let i=0; i<Bicicleta.allBicis.length; i++) {
        //si el id de una bicicleta coincide con el que se le pasa borra la bicicleta
        if (Bicicleta.allBicis[i].id == aBiciId) {
            //borra la bicicleta del array y si es necesario la remplaza
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}
Bicicleta.findById = function(aBiciId){
    //variable que almacena una bicicleta si su id coincide con el que se le pasa por parametro
    let aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    //si hay algo en aBici me devuelve la bicicleta
    if(aBici){
        return aBici;
    }else{
        //si no me muestra un mensaje de error 
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
    }
}*/

module.exports = mongoose.model ("Bicicleta", bicicletaSchema);