let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let reservaSchema = new Schema ({
    desde: Date,
    hasta: Date,
    bicicleta: {type: mongoose.Schema.Types.ObjectId, ref: "Bicicleta"},
    usuario: {type: mongoose.Schema.Types.ObjectId, ref: "Usuario"}
});

//Cuantos dias esta reservada la bicicleta
reservaSchema.methods.diasDeReserva = function() {
    return moment(this.hasta.diff(moment(this.desde), "days") + 1);
};

reservaSchema.statics.allReservas = function (cb) {
    return this.find( {}, cb);
};

module.exports = mongoose.model ("Reserva", reservaSchema);