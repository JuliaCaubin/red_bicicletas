let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Reserva = require("./Reserva");

let usuarioSchema = new Schema ({
    nombre: String
});

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    let reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.statics.allUsers = function (cb) {
    return this.find( {}, cb);
};

usuarioSchema.statics.add = function (aUser, cb) {
    return this.create(aUser, cb);
};

module.exports = mongoose.model ("Usuario", usuarioSchema);