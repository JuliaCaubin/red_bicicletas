const Reserva = require("../../models/Reserva.js");
let Usuario = require("../../models/Reserva.js");

exports.reserva_list = function (req, res) {
    Reserva.allReservas(function (err, reser){
        if (err) { 
            res.status(500).send(err.message);
        }
        res.status(200).json({
            reserva: reser
        });
    });
};