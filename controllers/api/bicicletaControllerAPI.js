let Bicicleta = require("../../models/Bicicleta");

exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function (err, bicis){
        if (err) { 
            res.status(500).send(err.message);
        }
        res.status(200).json({
            bicicletas: bicis
        });
    });
};

exports.bicicleta_create = function (req, res) {
    let bici = new Bicicleta ({
        bicicletaID: req.body.bicicletaID,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: req.body.ubicacion
    });

    Bicicleta.add(bici, function (err, newBici) {
        if (err) {
            res.status(500).send(err.message);
        }
        res.status(201).send(newBici);
    });
};

exports.bicicleta_delete = function(req,res){
    Bicicleta.removeById(req.body._id, function(err, bici){
        if (err) {
            res.status(500).send(err.message);
        }
        res.status(201).send();
    })
};

exports.bicicleta_update = function(req,res){
    Bicicleta.findById(req.body._id, req.body, function(err, bici){
        if (err) {
            res.status(500).send(err.message);
        }
        res.status(201).send(bici);
    })
}


/*exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    })
};
exports.bicicleta_create = function(req,res){
    let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.latitud, req.body.longitud];

    Bicicleta.add(bici);

    res.status(201).json({
        bicicleta:bici
    })
}
exports.bicicleta_delete = function(req,res){
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
};
exports.bicicleta_update_post = function(req,res){
    let bici = Bicicleta.findById(req.body.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.latitud, req.body.longitud];
    res.status(203).json({
        bicicleta:bici
    })
}*/
