let Usuario = require("../../models/Usuario");

exports.usuario_reservar = function(req, res){
    Usuario.findById(req.body._id, function(err, usuario){
        if(err) res.status(500).send(err.message);
        console.log(usuario);
        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err){
            console.log("Reservada!!");
            res.status(200).send();
        });
    });
};

exports.user_list = function (req, res) {
    Usuario.allUsers(function (err, user){
        if (err) { 
            res.status(500).send(err.message);
        }
        res.status(200).json({
            usuario: user
        });
    });
};

exports.user_create = function (req, res) {
    let user = new Usuario ({
        nombre: req.body.nombre
    });

    Usuario.add(user, function (err, newUsu) {
        if (err) {
            res.status(500).send(err.message);
        }
        res.status(201).send(newUsu);
    });
};