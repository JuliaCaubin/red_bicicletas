//llamamos al modelo Bicicleta 
let Bicicleta = require("../models/Bicicleta");

exports.bicicleta_list = function(req,res){
    Bicicleta.allBicis(function (err, bicis){
        if (err) { 
            res.status(500).send(err.message);
        }
        res.render("bicicletas/index", {
            bicicletas: bicis
        });
    });
}

//metodo que muestra el formulario (se queda igual)
exports.bicicleta_create_get = function(req,res) {
    res.render("bicicletas/create");
}

exports.bicicleta_create_post = function(req,res) {
    let bici = new Bicicleta ({
        bicicletaID: req.body.bicicletaID,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.latitud, req.body.longitud]
    });

    Bicicleta.add(bici, function (err, newBici) {
        if (err) {
            res.status(500).send(err.message);
        }
        res.redirect("/bicicletas");
    });
}

exports.bicicleta_delete_post = function(req,res) {
    Bicicleta.removeById(req.body.id, function(err, bici){
        if (err) {
            res.status(500).send(err.message);
        }
        res.redirect("/bicicletas");
    })
}

exports.bicicleta_update_get = function(req,res){
    console.log(req.body.id);
    Bicicleta.findById(req.params.id, req.body, function(err, bici){
        if (err) {
            res.status(500).send(err.message);
        }
        res.render("bicicletas/update", {
            bici
        });
    })
}

exports.bicicleta_update_post = function(req,res){

    let newBici =  {
        id: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.latitud, req.body.longitud]
    };

    console.log(newBici);

    Bicicleta.findById(req.params.id, newBici, function(err, bici){
        if (err) {
            res.status(500).send(err.message);
        }
        res.redirect("/bicicletas");
    })
}



/*exports.bicicleta_list = function(req,res){
    res.render("bicicletas/index", {bicis: Bicicleta.allBicis});
}
//metodo que realiza el envio del formulario con la nueva bicicleta
exports.bicicleta_create_post = function(req,res) {
    let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.latitud, req.body.longitud];
    //añade la bicicleta al array
    Bicicleta.add(bici);
    //vuelve a la pagina principal
    res.redirect("/bicicletas");
}
//metodo que elimina la bicicleta
exports.bicicleta_delete_post = function(req,res) {
    Bicicleta.removeById(req.body.id);
    res.redirect("/bicicletas");
}
//metodo que muestra el formulario con los datos de la bicicleta que vamos a modificar
exports.bicicleta_update_get = function(req,res){
    console.log(req.params.id);
    let bici = Bicicleta.findById(req.params.id);
    res.render("bicicletas/update", {bici});
}
//metodo que envia el formulario con los datos cambiados de la bicicleta
exports.bicicleta_update_post = function(req,res){
    let bici = Bicicleta.findById(req.body.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.latitud, req.body.longitud];
    res.redirect("/bicicletas");
}*/