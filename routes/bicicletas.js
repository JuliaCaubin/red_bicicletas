//incluimos express
var express = require('express');
//incluimos router
var router = express.Router();

//incluimos el controlador de bicicletas
let bicicletaController = require('../controllers/bicicleta');

//creamos la ruta llamando al metodo del controlador que muestra las bicicletas
router.get("/", bicicletaController.bicicleta_list);
//creamos la ruta /create llamando al metodo del controlador que muestra el formulario para añadir una nueva bicicleta
router.get("/create", bicicletaController.bicicleta_create_get);
//creamos la ruta /create llamando al metodo del controlador que realiza el envio con la nueva bicicleta
router.post("/create", bicicletaController.bicicleta_create_post);
//creamos la ruta en la que se la pasa un id y llama al metodo del controlador que borra una bicicleta por su id
router.post("/:id/delete", bicicletaController.bicicleta_delete_post);
//creamos la ruta que se le pasa un id y llama al metodo del controlador que devuelve el formulario con los cambios ya hechos en la bicicleta
router.post("/:id/update", bicicletaController.bicicleta_update_post);
//creamos la ruta que se le pasa un id y muestra el formulario con los datos de la bicicleta para poder actualizarlos
router.get("/:id/update", bicicletaController.bicicleta_update_get);

//exportamo el modulo para que se pueda acceder desde toda la aplicacion
module.exports = router;