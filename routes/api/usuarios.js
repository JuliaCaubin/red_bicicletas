const { Router } = require("express");
let express = require("express");
let router = express.Router();
let usuarioConttrollerAPI = require("../../controllers/api/usuariosControllerAPI");


router.post("/reservar", usuarioConttrollerAPI.usuario_reservar);

router.get("/",usuarioConttrollerAPI.user_list);
router.post("/create", usuarioConttrollerAPI.user_create);

module.exports = router;